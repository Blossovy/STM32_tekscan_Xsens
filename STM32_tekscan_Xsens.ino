//modified Wire library to 64bits, Xsens received all packets
// for STM32
//USART_TX_BUF_SIZE               128
//USART_RX_BUF_SIZE               128
//wire buffer size64
#include "XSensS1.h"
#include <Wire.h>

#define fsrpinA PA0
#define fsrpinB PA1
#define fsrpinC PA2
#define fsrpinD PA3
#define fsrpinE PA4
#define fsrpinF PA5
#define fsrpinG PA6
#define batLvl PA7
#define btState PA8

//Initializing XSens at ADDRESS
XSensS1 xsens(0x6B);
XSensS1 xsens2(0x6A);

byte IMU_state, IMU2_state;
boolean imuRead = true;
uint8_t address = 0x6B;
uint8_t address2 = 0x6A;

boolean btConnected = false;
//LED lights up when receving data thru BT 
unsigned long ledStart;
boolean ledON = false;
int ledDelay = 100;

int fsrA,fsrB,fsrC,fsrD,fsrE,fsrF,fsrG; 
int printDelay = 18;

int batteryDelay = 10000;
int batteryCount;
boolean batteryFirstFlag = true;

int motorDur,motorStartT;
int motorPendDur = 0;
boolean motorActive = false;
boolean motorInitDone = false;

void setup() {
  disableDebugPorts();
  pinMode(fsrpinA, INPUT_ANALOG);
  pinMode(fsrpinB, INPUT_ANALOG);
  pinMode(fsrpinC, INPUT_ANALOG);
  pinMode(fsrpinD, INPUT_ANALOG);
  pinMode(fsrpinE, INPUT_ANALOG);
  pinMode(fsrpinF, INPUT_ANALOG);
  pinMode(fsrpinG, INPUT_ANALOG);
  pinMode(batLvl, INPUT_ANALOG);
  pinMode(btState, INPUT);
  pinMode(PC13, OUTPUT);
  pinMode(PB3, OUTPUT);
  pinMode(PB4, OUTPUT);
  digitalWrite(PB3, LOW);
  digitalWrite(PB4, LOW);
  digitalWrite(PC13,HIGH);
  Serial.begin(500000);
  Serial3.begin(115200); //for bluetooth
  delay(300);
  motorInit();
  Wire.begin();
  Wire.setClock(100000);
  xsens.begin();
  xsens2.begin(); 
  delay(100);
  wireScan();
}

void loop() {
  int startL = millis();
  //update data every two loops 
  if (imuRead){
    if(IMU_state == 0){
      xsens.updateMeasures();
    }
    if(IMU2_state == 0){   
      xsens2.updateMeasures();
    }
  }
  
  //check for bluetooth state change !connect to connected
  if ((!btConnected) && (digitalRead(btState)==HIGH)){
    batteryFirstFlag = true;
  } 
  
  //print data if bluetooth is connected
  btConnected = (digitalRead(btState)==HIGH)?true:false;
  if (btConnected){
    receive();
    Serial3.flush();
    Serial3.print("T=");
    Serial3.print(startL);
    Serial3.print(";");
    FSR();
    if (imuRead){
      if(IMU_state == 0)
        IMU();
      if(IMU2_state == 0)
        IMU2();
    }
    battery();
    Serial3.println("#");
  }

  imuRead = !imuRead;
  motorloop();
  if ((millis()-startL) < printDelay){
      delay((printDelay - (millis() - startL)));
    }
}

void battery(){
      //print battery if it is first output
    if (batteryFirstFlag){
        batteryCount = millis();
        batteryFirstFlag = false;
        Serial3.print("B=");
        Serial3.print(analogRead(batLvl));
        Serial3.print(";");
    }
   
    if ((millis()- batteryCount )> batteryDelay){
      batteryCount = millis();
      Serial3.print("B=");
      Serial3.print(analogRead(batLvl));
      Serial3.print(";");
    }
}
void receive(){
  //PC13 led blink when data received
    if (Serial3.available()) {
    char btReceiver = Serial3.read();
  //  Serial.write(btReceiver);
    ledStart = millis(); 
    ledON = true;
    digitalWrite(PC13,LOW);
    motor (btReceiver);
  }
  if (ledON == true && (millis() - ledStart) > ledDelay){
    digitalWrite(PC13,HIGH);
    ledON = false;
  }
  
//  if (Serial.available())
//    Serial3.write(Serial.read());
}

void motor(char cmd){
  switch (cmd){
    case '1':
      motorPendDur = 0;
      //Serial3.print("get1");
      motorInit();
      break;
    case '2':
      motorPendDur = 800;
      //Serial3.print("get2");
      motorInit();
      break;
    case '3':
      motorPendDur = 3000;
      //Serial3.print("get3");
      motorInit();
      break;    
    case '4':
      motorPendDur = 500;
      //Serial3.print("get3");
      motorInit();
      break;
  }
  
}
void motorInit(){
  motorInitDone = false;
  motorStartT = millis();
  motorDur = 3000;
  digitalWrite(PB3, HIGH);
  //Serial3.print("Initi ");
}

void motorloop(){
  //turn off motor if time is out
  if (motorDur > 0){
    if ((millis()- motorStartT)> motorDur){
        //Serial3.print("timeOut ");
        digitalWrite(PB3, LOW);
        if (!motorInitDone){
          //Serial3.print("InitDone ");
          motorInitDone = true;
          motorDur = motorPendDur;
          if (motorDur > 0){
             //Serial3.print("Reverse ");
             motorStartT = millis();
             digitalWrite(PB4, HIGH);
             motorPendDur = 0;
          }
      }else{
        digitalWrite(PB4, LOW);
        motorDur = 0;
        //Serial3.print("ReverseStop ");
      }
    }
  }
}

void wireScan(){
  Wire.beginTransmission(address);
  IMU_state = Wire.endTransmission();
  Wire.beginTransmission(address2);
  IMU2_state = Wire.endTransmission();
}

void FSR(){
  fsrA = analogRead(fsrpinA);
  fsrB = analogRead(fsrpinB);
  fsrC = analogRead(fsrpinC);
  fsrD = analogRead(fsrpinD);
  fsrE = analogRead(fsrpinE);
  fsrF = analogRead(fsrpinF);
  fsrG = analogRead(fsrpinG);
  
  Serial3.print("F="); Serial3.print(fsrA);
  Serial3.print(","); Serial3.print(fsrB);
  Serial3.print(","); Serial3.print(fsrC);
  Serial3.print(","); Serial3.print(fsrD);
  Serial3.print(","); Serial3.print(fsrE);
  Serial3.print(","); Serial3.print(fsrF);
  Serial3.print(","); Serial3.print(fsrG);
  Serial3.print(";");
}
void IMU(){
//  Serial3.print("Pack= ");
//  Serial3.print(xsens.getPackCounter());
  
  Serial3.print("I=");
  for(int i = 0 ; i < 3; ++i){
    Serial3.print(xsens.getFAccel()[i]);
    Serial3.print(",");
  }
  
  for(int i = 0 ; i < 3; ++i){
    Serial3.print(xsens.getRot()[i]);
    Serial3.print(",");
  }

  for(int i = 0 ; i < 2; ++i){
    Serial3.print(xsens.getEulerd()[i]);
    Serial3.print(",");
  }
  Serial3.print(xsens.getEulerd()[2]);
  Serial3.print(";");
} 

 void IMU2(){
//  Serial3.print("Pack= ");
//  Serial3.print(xsens2.getPackCounter());
  
  Serial3.print("II= ");
  for(int i = 0 ; i < 3; ++i){
    Serial3.print(xsens2.getFAccel()[i]);
    Serial3.print(",");
  }
  
  for(int i = 0 ; i < 3; ++i){
    Serial3.print(xsens2.getRot()[i]);
    Serial3.print(",");
  }

  for(int i = 0 ; i < 2; ++i){
    Serial3.print(xsens2.getEulerd()[i]);
    Serial3.print(",");
  }
  Serial3.print(xsens2.getEulerd()[2]);
  Serial3.print(";");
}

